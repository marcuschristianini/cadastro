#Visitando o site de cadastro
Dado(/^que esteja no site$/) do
	visit "https://www.phptravels.net/admin"
end

#Realizando Login e senha 
Quando (/^realizar o login e senha$/) do
	fill_in ('Email', :with => 'adddmin@phptravels.com')
	fill_in ('Password', :with => 'demoadmin')
end

# Acesso e interação no menu
E (/^acessar o menu$/) do
	click_button('//input[@id=sidebar Collapse]')
end

E (/^acessar accounts$/) do
	click_link ('a href="https://www.phptraves.net/admin/accounts/addmins/"')
end

E (/^acessar Suppliers$/) do
	click_link('a href="https://www.phptraves.net/admin/accounts/supplires/')
end

E (/^acessar add$/) do
	click_button('add')
end

E (/^preencher nome$/) do
	fill_in('fname', :with => "marcus")
end

E (/^preencher ultimo nome$/) do
	fill_in('lname', :with => "christianini")
end

E (/^preencher email$/) do
	fill_in('email', :with => "cra@www.com")
end

E (/^preencher password$/) do
	fill_in('password', :with => "123456")
end
E (/^preencher Telefone movel$/) do
	fill_in('mobile', :with => "933333333")
end

E(/^selecionar country$/) do
	select('//*[@id="select2-drop"]', :from => "Brazil")
end

E(/^preencher endereco1$/) do
	fill_in('address1', :with => "Rua blablabla 123")
end

E(/^preencher endereco2$/) do
	fill_in('address2', :with => "albalbalb 321")
end

Entao(/^clicar em enviar$/) do
	click_button('submit')
	assert_text('cra@www.com')
end
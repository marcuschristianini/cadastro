require 'rspec'
require 'page-object'
require 'data_magic'
require 'selenium-webdriver'
require 'capybara'

World(PageObject::PageFactory)

#Configuração o drive
Capybara.regster_driver :selenium do |app|
	Capybara::Selenium::Driver.new(app,:browzer => :chrome)
end

# Set driver como padrao
Copybara.defaut_driver = :selenium

# Timeout padrão da execução
Capybara.defaut_max_time = 20

# Maximizando tela
Capybara.page.driver.browzer.manage.window.maximize

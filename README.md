Readme

************************************************************
Após o ambiente pronto e realizar o test.
Não conheco muito da linguagem mas vamos aprimorando com o tempo


*************************************************************

Projeto
Criar um único projeto contendo:

"Readme" com orientações de como instalar o ambiente e executar os testes
A solução dos desafios propostos a seguir
Desafios - WebSite
Fazer o cadastro de um novo ACCOUNTS -> SUPPLIER no site https://www.phptravels.net/admin e validar que o mesmo foi adicionado a lista de SUPPLIERS cadastrados. Dados para login:
usuário: admin@phptravels.com
senha: demoadmin
 
Obrigatório:: Utilizar conceito de PageObjects. Diferencial: Geração dinâmica de dados para efetuar o cadastro.
Envio dos Desafios
Primeiramente, efetue um fork deste repositório. Desenvolva e faça os commits no seu fork. Quando terminar, envie um Pull Request através do BitBucket.

Dica
Caso não consiga finalizar 100% do projeto, nos envie mesmo assim!

Nós avaliamos diversos itens como lógica, estrutura, padrões utilizados, entre outras coisas